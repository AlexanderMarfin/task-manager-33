package ru.tsc.marfin.tm.api.endpoint;

public interface IUserEndpointClient extends IEndpointClient, IUserEndpoint {
}
