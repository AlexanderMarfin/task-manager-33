package ru.tsc.marfin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "argument";

    @NotNull
    public static final String DESCRIPTION = "Show application arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final AbstractCommand command: commands) {
            @NotNull final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
