package ru.tsc.marfin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start project by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectEndpoint().changeProjectStatusById(new ProjectChangeStatusByIdRequest(id, Status.IN_PROGRESS));
    }

}
