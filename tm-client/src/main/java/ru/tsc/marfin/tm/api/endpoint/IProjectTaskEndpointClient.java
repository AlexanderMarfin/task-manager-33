package ru.tsc.marfin.tm.api.endpoint;

public interface IProjectTaskEndpointClient extends IEndpointClient, IProjectTaskEndpoint {
}
