package ru.tsc.marfin.tm.api.endpoint;

public interface IAuthEndpointClient extends IEndpointClient, IAuthEndpoint {
}
