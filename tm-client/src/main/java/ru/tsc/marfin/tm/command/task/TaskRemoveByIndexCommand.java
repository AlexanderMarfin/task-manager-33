package ru.tsc.marfin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.dto.request.TaskRemoveByIndexRequest;
import ru.tsc.marfin.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-index";

    @NotNull
    public static final String DESCRIPTION = "Remove task by index";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskEndpoint().removeTaskByIndex(new TaskRemoveByIndexRequest(index));
    }

}
