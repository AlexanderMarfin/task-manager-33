package ru.tsc.marfin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.command.AbstractCommand;
import ru.tsc.marfin.tm.dto.Domain;
import ru.tsc.marfin.tm.dto.request.ProjectListRequest;
import ru.tsc.marfin.tm.dto.request.TaskListRequest;


public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BINARY = "../data.bin";

    @NotNull
    public static final String FILE_BASE64 = "../data.base64";

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "../data_fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "../data_fasterxml.xml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "../data_jaxb.json";

    @NotNull
    protected static final String FILE_JAXB_XML = "../data_jaxb.xml";

    @NotNull
    protected static final String FILE_FASTERXML_YAML = "../data_fasterxml.yaml";

    @NotNull
    public static final String FILE_BACKUP = "../backup.base64";

    @NotNull
    protected static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXB_CONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected static final String APPLICATION_JSON = "application/json";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
//        domain.setProjects(serviceLocator.getProjectEndpoint().listProject(new ProjectListRequest()).getProjects());
//        domain.setTasks(serviceLocator.getTaskEndpoint().listTask(new TaskListRequest()).getTasks());
//        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
//        serviceLocator.getProjectService().set(domain.getProjects());
//        serviceLocator.getTaskService().set(domain.getTasks());
//        serviceLocator.getUserService().set(domain.getUsers());
//        serviceLocator.getAuthService().logout();
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

}
