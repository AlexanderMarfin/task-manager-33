package ru.tsc.marfin.tm.api.endpoint;

public interface ITaskEndpointClient extends IEndpointClient, ITaskEndpoint {
}
