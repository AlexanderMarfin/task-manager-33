package ru.tsc.marfin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.dto.request.UserUnlockRequest;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-unlock";

    @NotNull
    public static final String DESCRIPTION = "Unlock user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserEndpoint().unlockUser(new UserUnlockRequest(login));
    }

}
