package ru.tsc.marfin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start project by index";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectEndpoint().changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(index, Status.IN_PROGRESS));
    }

}
