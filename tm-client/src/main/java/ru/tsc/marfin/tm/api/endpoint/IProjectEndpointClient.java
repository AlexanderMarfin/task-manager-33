package ru.tsc.marfin.tm.api.endpoint;

public interface IProjectEndpointClient extends IEndpointClient, IProjectEndpoint {
}
