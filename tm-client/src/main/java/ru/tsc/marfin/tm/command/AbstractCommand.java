package ru.tsc.marfin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.model.ICommand;
import ru.tsc.marfin.tm.api.service.IPropertyService;
import ru.tsc.marfin.tm.api.service.IServiceLocator;
import ru.tsc.marfin.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void execute();

    @NotNull
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += " - " + description;
        return result;
    }

}
