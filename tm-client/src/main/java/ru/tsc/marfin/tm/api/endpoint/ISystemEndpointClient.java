package ru.tsc.marfin.tm.api.endpoint;

public interface ISystemEndpointClient extends IEndpointClient, ISystemEndpoint {
}
