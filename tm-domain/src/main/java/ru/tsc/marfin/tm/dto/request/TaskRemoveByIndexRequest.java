package ru.tsc.marfin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskRemoveByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

}
