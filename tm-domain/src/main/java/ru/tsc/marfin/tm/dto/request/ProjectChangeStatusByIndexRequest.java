package ru.tsc.marfin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

}
