package ru.tsc.marfin.tm.exception.entity;

public class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }
}
