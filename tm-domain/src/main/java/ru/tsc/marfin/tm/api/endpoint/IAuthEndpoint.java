package ru.tsc.marfin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.dto.request.UserLoginRequest;
import ru.tsc.marfin.tm.dto.request.UserLogoutRequest;
import ru.tsc.marfin.tm.dto.request.UserShowProfileRequest;
import ru.tsc.marfin.tm.dto.response.UserLoginResponse;
import ru.tsc.marfin.tm.dto.response.UserLogoutResponse;
import ru.tsc.marfin.tm.dto.response.UserShowProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IAuthEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String SPACE = "http://endpoint.tm.marfin.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(
            @NotNull final String host, @NotNull final String port
    ) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLoginRequest request
    );

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    UserShowProfileResponse profile(
            @WebParam(name = "request", partName = "request")
            @NotNull UserShowProfileRequest request
    );

}
