package ru.tsc.marfin.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.model.User;

@NoArgsConstructor
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
