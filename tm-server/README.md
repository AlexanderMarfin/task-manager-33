# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Alexander Marfin

* **E-Mail**: amarfin@t1-consulting.ru

## SOFTWARE

* OpenJDK 8

* Intelliji Idea

* MS Windows 7 x64

## HARDWARE

* **RAM**: 8gb

* **CPU**: Intel Core i3-2310M 2.1 GHz

* **HDD**: 512Gb

## BUILD APPLICATION

```bash
mvn clean install
```

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```