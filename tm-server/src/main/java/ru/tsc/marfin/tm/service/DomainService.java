package ru.tsc.marfin.tm.service;

import ru.tsc.marfin.tm.api.service.IDomainService;

public class DomainService implements IDomainService {

    @Override
    public void loadDataBackup() {

    }

    @Override
    public void saveDataBackup() {

    }

    @Override
    public void loadDataBase64() {

    }

    @Override
    public void saveDataBase64() {

    }

    @Override
    public void loadDataBinary() {

    }

    @Override
    public void saveDataBinary() {

    }

    @Override
    public void loadDataJsonFasterXml() {

    }

    @Override
    public void saveDataJsonFasterXml() {

    }

    @Override
    public void loadDataJsonJaxb() {

    }

    @Override
    public void saveDataJsonJaxb() {

    }

    @Override
    public void loadDataXmlFasterXml() {

    }

    @Override
    public void saveDataXmlFasterXml() {

    }

    @Override
    public void loadDataXmlJaxb() {

    }

    @Override
    public void saveDataXmlJaxb() {

    }

    @Override
    public void loadDataYamlFasterXml() {

    }

    @Override
    public void saveDataYamlFasterXml() {

    }

}
