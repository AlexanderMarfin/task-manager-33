package ru.tsc.marfin.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.endpoint.IUserEndpoint;
import ru.tsc.marfin.tm.api.service.IServiceLocator;
import ru.tsc.marfin.tm.api.service.IUserService;
import ru.tsc.marfin.tm.dto.request.AbstractUserRequest;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.exception.system.AccessDeniedException;
import ru.tsc.marfin.tm.model.User;


import java.util.Optional;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        Optional.ofNullable(request).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(role).orElseThrow(AccessDeniedException::new);
        @Nullable final String userId = request.getUserId();
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        Optional.ofNullable(user).orElseThrow(AccessDeniedException::new);
        @Nullable final Role roleUser = user.getRole();
        final boolean check = roleUser.equals(role);
        if (!check) throw new AccessDeniedException();
    }

    protected  void check(@Nullable final AbstractUserRequest request) {
        Optional.ofNullable(request).orElseThrow(AccessDeniedException::new);
        @Nullable final String userId = request.getUserId();
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
    }

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
