package ru.tsc.marfin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.endpoint.IUserEndpoint;
import ru.tsc.marfin.tm.api.service.IServiceLocator;
import ru.tsc.marfin.tm.api.service.IUserService;
import ru.tsc.marfin.tm.dto.request.*;
import ru.tsc.marfin.tm.dto.response.*;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.marfin.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserChangePasswordRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String password = request.getPassword();
        @Nullable User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserRegistryRequest request
    ) {
        @Nullable String login = request.getLogin();
        @Nullable String password = request.getPassword();
        @Nullable String email = request.getEmail();
        @Nullable User user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserUpdateProfileRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String lastName = request.getLastName();
        @Nullable String firstName = request.getFirstName();
        @Nullable String middleName = request.getMiddleName();
        @Nullable User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

}
